package test;


import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import sh4j.model.browser.SClass;
import sh4j.model.browser.SPackage;
import sh4j.model.browser.SProject;
import sh4j.parser.SClassParser;
import sh4j.model.dataCollector.SClassCollector;
import sh4j.model.dataCollector.SLinesOfCodeCollector;
import sh4j.model.dataCollector.SWarningCollector;

public class SDataCollectorTest {

  
  @Test
  public void linesOfCodeTest() {
    SLinesOfCodeCollector locCollector = new SLinesOfCodeCollector();
    List<SClass> cls = SClassParser.parse("class A{ public void method4lines() {x++;} } class B{ int x = 2; public void method4lines() { x++;} public void method5lines() { x += 3; x += 4; } }");
    SProject project = new SProject();
    SPackage pkg = new SPackage("pack");
    project.addPackage(pkg);
    for (SClass c : cls) {
      pkg.addClass(c);
    }
    locCollector.collect(project);
    assertEquals(13,locCollector.getLoC());
    assertEquals(0,locCollector.getLoC());
    
    
  }
  
  @Test
  public void warningTest() {
    SWarningCollector warningCollector = new SWarningCollector();    
    List<SClass> cls = SClassParser.parse("class A{ public void method4lines() {x++;} } class B{ int x = 2; public void method4lines() { x++;} public void method5lines() { x += 3; x += 4; } }");
    SProject project = new SProject();
    SPackage pkg = new SPackage("pack");
    project.addPackage(pkg);
    for (SClass c : cls) {
      pkg.addClass(c);
    }
    
    warningCollector.collect(project);
    assertEquals(0,warningCollector.getWarnings());    
    
    cls = SClassParser.parse("class A{ public void method41lines() {x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;} } class B{ int x = 2; public void method4lines() { x++;} public void method5lines() { x += 3; x += 4; } }");
    project = new SProject();
    pkg = new SPackage("pack");
    project.addPackage(pkg);
    for (SClass c : cls) {
      pkg.addClass(c);
    }

    warningCollector.collect(project);
    assertEquals(1,warningCollector.getWarnings());  
    assertEquals(0,warningCollector.getWarnings());     
       
    cls = SClassParser.parse("class A{ public void method41lines() {x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;} } class B{ int x = 2; public void method2Manylines() { x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;} public void method5lines() { x += 3; x += 4; x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++;x++; } }");
    project = new SProject();
    pkg = new SPackage("pack");
    project.addPackage(pkg);
    for (SClass c : cls) {
      pkg.addClass(c);
    }

    warningCollector.collect(project);
    assertEquals(3,warningCollector.getWarnings());  
    assertEquals(0,warningCollector.getWarnings());        
   
  }
  
  @Test
  public void classTest() {
    SClassCollector classCollector = new SClassCollector();    
    List<SClass> cls = SClassParser.parse("class A{ public void method4lines() {x++;} } class B{ int x = 2; public void method4lines() { x++;} public void method5lines() { x += 3; x += 4; } }");
    SProject project = new SProject();
    SPackage pkg = new SPackage("pack");
    project.addPackage(pkg);
    for (SClass c : cls) {
      pkg.addClass(c);
    }
    
    classCollector.collect(project);
    assertEquals(2,classCollector.getClassNumber());  
    assertEquals(0,classCollector.getClassNumber());
    
  }
}
