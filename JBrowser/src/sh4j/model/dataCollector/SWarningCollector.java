package sh4j.model.dataCollector;

import sh4j.model.browser.SClass;
import sh4j.model.browser.SMethod;
import sh4j.model.browser.SPackage;
import sh4j.model.browser.SProject;

import java.awt.Color;


/**
 * Collects total amount of warnings (methods with 30+ LoC) in project.
 * 
 * @author Alberto
 *
 */
public class SWarningCollector extends SDataCollector {
  private int warnings;
  
  public SWarningCollector() {
    warnings = 0;
  }
  
  /**
   * Collects warnings from SClass' SMethods.
   * 
   * @param clss SClass to collect from.
   */
  public void collectClassData(SClass clss) {
    for (SMethod methods : clss.methods()) {
      if (methods.background() != Color.WHITE) {
        warnings++;
      }
    }

  }


  
  /**
   * Gets warnings, then RESETS warning count to make the same object reusable.
   * 
   * @return warnings number.
   */
  
  public int getWarnings() {
    int aux = warnings;
    warnings = 0;
    return aux;
  }

  @Override
  public void collectPackageData(SPackage pkg) {
    pkg.background();
    
  }

  @Override
  public void collectProjectData(SProject project) {
    project.background();
  }
  

}
