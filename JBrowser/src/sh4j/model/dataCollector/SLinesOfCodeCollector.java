package sh4j.model.dataCollector;

import sh4j.model.browser.SClass;
import sh4j.model.browser.SMethod;
import sh4j.model.browser.SPackage;
import sh4j.model.browser.SProject;

/**
 * Collects total amount of LoC in project.
 * 
 * @author Alberto
 *
 */
public class SLinesOfCodeCollector extends SDataCollector {
  private int linesOfCode;
  
  public SLinesOfCodeCollector() {
    linesOfCode = 0;
  }
  
  /**
   * Collects lines of code from the given SClass.
   * 
   * @param clss SClass to get LoC from.
   */
  public void collectClassData(SClass clss) {
    for (SMethod methods : clss.methods()) {
      linesOfCode += methods.getLinesOfCode();
    }
  }
  
  /**
   * Gets lines of code, then RESETS lines of code count to make the same object reusable.
   * 
   * @return lines of code.
   */
  public int getLoC() {
    int aux = linesOfCode;
    linesOfCode = 0;
    return aux;
  }

  @Override
  public void collectPackageData(SPackage pkg) {
    pkg.background();
  }

  @Override
  public void collectProjectData(SProject project) {
    project.background();
    
  }

}
