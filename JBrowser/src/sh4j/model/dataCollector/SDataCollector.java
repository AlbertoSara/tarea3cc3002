package sh4j.model.dataCollector;


import sh4j.model.browser.SClass;
import sh4j.model.browser.SPackage;
import sh4j.model.browser.SProject;

/**
 * Used to create SDataCollector subclasses. Iterates on projects to collect different data.
 * 
 * @author Alberto
 *
 */
public abstract class SDataCollector {
  
  /**
   * Collects data from project.
   * 
   * @param project Project to collect data from.
   */
  public void collect(SProject project) {
    project.acceptCollector(this);
  }
  
  public abstract void collectClassData(SClass clss);
  
  public abstract void collectPackageData(SPackage pkg);
  
  public abstract void collectProjectData(SProject project);
  
}
