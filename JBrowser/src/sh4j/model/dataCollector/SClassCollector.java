package sh4j.model.dataCollector;

import sh4j.model.browser.SClass;
import sh4j.model.browser.SPackage;
import sh4j.model.browser.SProject;

/** 
 * Collects total number of classes in project.
 * 
 * @author Alberto
 *
 */
public class SClassCollector extends SDataCollector {
  private int numClasses;
  
  public SClassCollector() {
    numClasses = 0;
  }
  
  public void collectClassData(SClass clss) {
    numClasses++;
  }

  
  /**
   * Gets number of classes, then RESETS number of classes count to make the same object reusable.
   * 
   * @return number of classes.
   */
  public int getClassNumber() {
    int aux = numClasses;
    numClasses = 0;
    return aux;
  }

  @Override
  public void collectPackageData(SPackage pkg) {
    numClasses++;
    numClasses--;
  }

  @Override
  public void collectProjectData(SProject project) {
    numClasses++;
    numClasses--;
  }

}
