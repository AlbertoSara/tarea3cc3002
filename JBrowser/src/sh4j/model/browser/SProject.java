package sh4j.model.browser;

import sh4j.model.command.SCommand;
import sh4j.model.dataCollector.SDataCollector;

import java.awt.Color;
import java.awt.Font;
import java.util.ArrayList;
import java.util.List;

/**
 * Create projects for JBrowse.
 */
public class SProject implements SObject {
  
  private List<SPackage> packages;
  private String icon;
  private Color background;
  private Font font;
  
  public SProject() {
    packages = new ArrayList<SPackage>();
  }

  public void addPackage(SPackage pack) {
    packages.add(pack);
  }

  public List<SPackage> packages() {
    return packages;
  }
  
  /**
   * Sets the package list of the project to the one given.
   * @param pkglist New list of packages to use.
   */
  
  public void setPackages(List<SPackage> pkglist) {
    packages = pkglist;
  }
  
  
  /**
   * Get packages.
   * @param pkgName Name of package.
   * @return pkg Package.
   */
  public SPackage get(String pkgName) {
    for (SPackage pkg : packages) {
      if (pkg.toString().equals(pkgName)) {
        return pkg;
      }
    }
    return null;
  }
  
  public Font font() {
    return font;
  }
  
  public String icon() {
    return icon;
  }
  
  public Color background() {
    return background;
  }
  /**
   * Accepts commands, visitor pattern style, then sends 
   * command to packages contained by the project.
   * 
   * @param command SCommand object to use.
   */
  public void accept(SCommand command) {
    
    for (SPackage pkgs: this.packages()) {
      pkgs.accept(command);
    }

    command.visitProject(this);
    
  }
  
  /**
   * Accepts data collectors, then sends collector to packages contained by the project.
   * 
   * @param collector SDataCollector to use
   */
  
  public void acceptCollector(SDataCollector collector) {
    
    for (SPackage pkgs: this.packages()) {
      pkgs.acceptCollector(collector);
    }

    collector.collectProjectData(this);
  }
  
}
