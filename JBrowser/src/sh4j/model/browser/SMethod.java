package sh4j.model.browser;

import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.Modifier;
import sh4j.parser.model.SBlock;

import java.awt.Color;
import java.awt.Font;



/**
 * Class for Methods in JBrowser.
 */
public class SMethod implements SObject {
  private final MethodDeclaration declaration;
  private final SBlock body;
  private String icon;
  private Color background;
  private Font font;
  
  /**
   * Use this to instantiate SMethods.
   * @param node Header.
   * @param body Entire method.
   */
  public SMethod(MethodDeclaration node, SBlock body) {
    declaration = node;
    this.body = body;
  }
  /**
   * Give which modifier the class has.
   * @return String which kind of modifier it has.
   */
  public String modifier() {
    for (Object obj : declaration.modifiers()) {
      if (obj instanceof Modifier) {
        Modifier modifier = (Modifier) obj;
        return modifier.getKeyword().toString();
      }
    }
    return "default";
  }

  public String name() {
    return declaration.getName().getIdentifier();
  }

  public SBlock body() {
    return body;
  }

  public String toString() {
    return name();
  }
  
  public String icon() {
    this.setIcon();
    return icon;
  }
  
  private void setIcon() {
    icon = "./resources/" + this.modifier() + "_co.gif";
  }
    
  public void setFont() {
    font = new Font("Helvetica", Font.PLAIN, 12);
  }
  
  /**
   * Sets background color depending on lines of code amount.
   */
  private void setBackground() {
    if (this.getLinesOfCode() > 50) {
      background = Color.RED;
    } else {
      if (this.getLinesOfCode() > 30) {
        background = Color.YELLOW;
      } else {
        background = Color.WHITE;
      }
    }
  }
  
  /**
   * Gets how many lines of code the method has.
   * @return count How many lines of code it has.
   */
  public int getLinesOfCode() {
    String htmlBody = body().toString();
    return htmlBody.length() - htmlBody.replace("\n", "").length();
  }
  
  public Color background() {
    this.setBackground();
    return background;
  }
  
  public Font font() {
    return font;
  }
  
}
