package sh4j.model.browser;

import java.awt.Color;
import java.awt.Font;
/**
 * Extend to create more JBrowser classes.
 */
public interface SObject {
  public String icon();
  
  public Font font();
  
  public Color background();
  
}
