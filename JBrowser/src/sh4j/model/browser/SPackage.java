package sh4j.model.browser;

import sh4j.model.command.SCommand;
import sh4j.model.dataCollector.SDataCollector;

import java.awt.Color;
import java.awt.Font;
import java.util.ArrayList;
import java.util.List;
/**
 * Create packages for JBrowse.
 */
public class SPackage implements SObject, Comparable<SPackage> {

  private final String name;
  private List<SClass> classes;
  private String icon = "./resources/pack_empty_co.gif";
  private Font font;
  private Color background;
  
  public SPackage(String name) {
    classes = new ArrayList<SClass>();
    this.name = name;
  }

  public void addClass(SClass cls) {
    classes.add(cls);
    this.toggleIcon();
  }

  public List<SClass> classes() {
    return classes;
  }

  public String toString() {
    return name;
  }
  
  public String icon() {
    return icon;
  }
  
  public Font font() {
    return font;
  }
  
  public Color background() {
    return background;
  }
  /**
   * Sets the classes list of the package to the one given.
   * @param classlist New list of classes to use.
   */
  
  public void setClasses(List<SClass> classlist) {
    classes = classlist;
  }
  
  /**
   * Toggles the icon of the package depending if it is empty or not.
   */
  public void toggleIcon() {
    if (classes.isEmpty()) {
      icon = "./resources/pack_empty_co.gif";
    } else {
      icon = "./resources/package_mode.gif";
    }
  }
  
  /**
   * Accepts commands, visitor pattern style, then sends 
   * command to classes contained by the package.
   * 
   * @param command SCommand object to use.
   */
  public void accept(SCommand command) {
    
    for (SClass classes : this.classes()) {
      classes.acceptCommand(command);
    }

    command.visitPackage(this);
    
  }

  /**
   * Accepts data collectors.
   * 
   * @param collector SDataCollector to use
   */
  
  public void acceptCollector(SDataCollector collector) {
    
    for (SClass classes : this.classes()) {
      classes.acceptCollector(collector);
    }
    
    collector.collectPackageData(this);
  }
  
  @Override
  public int compareTo(SPackage spkg) {
    return this.toString().compareTo(spkg.toString());
  }
}
