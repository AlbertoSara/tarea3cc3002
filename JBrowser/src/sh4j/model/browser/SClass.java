package sh4j.model.browser;

import org.eclipse.jdt.core.dom.TypeDeclaration;

import sh4j.model.command.SCommand;
import sh4j.model.dataCollector.SDataCollector;

import java.awt.Color;
import java.awt.Font;
import java.util.Comparator;
import java.util.List;

/**
 * Representation of Classes in JBrowser.
 *
 */
public class SClass implements SObject, Comparator<SClass>, Comparable<SClass> {
  private final TypeDeclaration declaration;
  private final List<SMethod> methods;
  private String icon;
  private Font font;
  private Color background;
  private int superClasses;
  private boolean indents;
  
  /**
   * Use this to instantiate SClass objects.
   * @param td Type of the class.
   * @param ms List of SMethods contained by class.
   * 
   */
  public SClass(TypeDeclaration td, List<SMethod> ms) {
    declaration = td;
    methods = ms;
    superClasses = 0;
    indents = false;
  }

  /**
   * Special constructor that creates an "empty" SClass.
   */
  public SClass() {
    declaration = null;
    methods = null;
    superClasses = 0;
    indents = false;
  }
  
  public List<SMethod> methods() {
    return methods;
  }

  public String className() {
    return declaration.getName().getIdentifier();
  }

  public boolean isInterface() {
    return declaration.isInterface();
  }
  
  /**
   * Returns a string with the superclass name.
   *@return String, superclass name string.
   */
  
  public String superClass() {
    if (declaration.getSuperclassType() == null) {
      return "Object";
    }
    return declaration.getSuperclassType().toString();
  }

  /**
   * Modified toString that checks whether the name should be
   * returned indented or not.
   */
  public String toString() {
    if (this.indents) { 
      String ind = "";
      for (int i = 0; i < superClasses; i++) {
        ind = ind + "  ";
      }
      return ind + this.className();
    }
    return this.className();
    
  }
  
  public String icon() {
    this.setIcon();
    return icon;
  }
  
  /**
   * Sets the icon for the class depending if it is an interface or not.
   *
   */
  
  private void setIcon() {
    if (this.isInterface()) {
      icon = "./resources/int_obj.gif";
    } else {
      icon = "./resources/class_obj.gif";
    }
  }
  
  /**
   * Sets the font for the class depending if it is an interface or not.
   *
   */
  
  private void setFont() {
    if (this.isInterface()) {
      font = new Font("Helvetica", Font.ITALIC, 12);
    } else {
      font = new Font("Helvetica", Font.PLAIN, 12);
    }   
  }
  
  public Font font() {
    this.setFont();
    return font;
  }
  
  /**
   * Accepts commands, visitor pattern style.
   * 
   * @param command SCommand object to use.
   */
  public void acceptCommand(SCommand command) {
    command.visitClass(this);
  }
  
  /**
   * Accepts data collectors.
   * 
   * @param collector SDataCollector to use
   */
  
  public void acceptCollector(SDataCollector collector) {
    collector.collectClassData(this);
  }
  
  public Color background() {
    return background;
  }

  @Override
  public int compareTo(SClass arg0) {
    return this.toString().compareTo(arg0.toString());
  }

  @Override
  public int compare(SClass o1, SClass o2) {
    if (o1.superClass() == "Object") {
      return -1;
    }
    if (o2.superClass() == "Object") {
      return 1;
    }
    return o1.superClass().compareTo(o2.superClass());
  }
  
  public int superClasses() {
    return superClasses;
  } 
  
  public void setSuperClasses(int sprClss) {
    superClasses = sprClss;
  }
  
  public void setIndents(boolean ind) {
    indents = ind;
  }
}
