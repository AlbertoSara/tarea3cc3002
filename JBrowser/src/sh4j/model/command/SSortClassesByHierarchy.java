package sh4j.model.command;

import sh4j.model.browser.SClass;
import sh4j.model.browser.SPackage;
import sh4j.model.browser.SProject;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Sort class by Hierarchy.
 */
public class SSortClassesByHierarchy extends SCommand {
  
  private final boolean indents;
  
  public SSortClassesByHierarchy() {
    indents = true;
  }
  
  @Override
  public void executeOn(SProject project) {
    (new SSortClassesByName()).executeOn(project);
    for (final SPackage pkg: project.packages()) {
      Collections.sort(pkg.classes(), 
          new Comparator<SClass>() {
            @Override
            public int compare(SClass sclass1, SClass sclass2) {
              String path1 = path(pkg.classes(),sclass1); 
              String path2 = path(pkg.classes(),sclass2);
              return path1.compareTo(path2);
            }
        });
    }
    project.accept(this);
  }
  
  /**
   * Generates a string with all the superclasses of a class.
   * 
   * @param cls list of SClass to check for superclasses.
   * @param theClass class to make the path for.
   * @return String with the complete path.
   */
  public String path(List<SClass> cls,SClass theClass) {
    SClass parent = get(cls,theClass.superClass());
    if (parent == null) {
      return theClass.toString();
    } else {
      return path(cls,parent) + " " + theClass.toString();
    }
  }
  
  /**
   * Gets a class from a list of SClass that matches with 
   * given string.
   * 
   * @param cls List of SClass to search in.
   * @param theClass String with the name of the class.
   * @return SClass that matches, null if none.
   */
  public SClass get(List<SClass> cls, String theClass) {
    for (SClass c: cls) {
      if (c.toString().equals(theClass)) {
        return c;
      }
    }
    return null;
  }
  
  @Override
  public void visitPackage(SPackage pkg) {
    for (SClass clss : pkg.classes()) {
      String path = path(pkg.classes(),clss);
      clss.setSuperClasses(path.length() - path.replace(" ","").length());
      clss.setIndents(indents);
    } 
  }
  

  @Override
  public void visitClass(SClass clss) {
    clss.background();
  }

  @Override
  public void visitProject(SProject project) {
    project.background();
    
  }
  
}
