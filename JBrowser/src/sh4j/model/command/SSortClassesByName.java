package sh4j.model.command;

import sh4j.model.browser.SClass;
import sh4j.model.browser.SPackage;
import sh4j.model.browser.SProject;

import java.util.Collections;

/**
 * Sort classes by name.
 */
public class SSortClassesByName extends SCommand {
  
  private final boolean indents;
  
  public SSortClassesByName() {
    indents = false;
  }
  
  
  @Override
  public void executeOn(SProject project) {
    project.accept(this);
  }
  
  @Override
  public void visitPackage(SPackage pkg) {
    Collections.sort(pkg.classes());
  }
  
  @Override
  public void visitClass(SClass clss) {
    clss.setIndents(indents);
  }


  @Override
  public void visitProject(SProject project) {
    project.background();
  }
  
}
