package sh4j.model.command;


import sh4j.model.browser.SClass;
import sh4j.model.browser.SPackage;
import sh4j.model.browser.SProject;
/**
 * It represent a command that could be applied to a project.
 * 
 * @author juampi
 *
 */
public abstract class SCommand {

  public abstract void executeOn(SProject project);
  
  public abstract void visitPackage(SPackage pkg);
  
  public abstract void visitClass(SClass clss);
  
  public abstract void visitProject(SProject project);
  
  public String name() {
    return this.getClass().getName();
  }
}
