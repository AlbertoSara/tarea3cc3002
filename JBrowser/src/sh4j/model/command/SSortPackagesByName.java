package sh4j.model.command;

import sh4j.model.browser.SClass;
import sh4j.model.browser.SPackage;
import sh4j.model.browser.SProject;

import java.util.Collections;

/**
 * Sort packages by name.
 */
public class SSortPackagesByName extends SCommand {
  
  private final boolean indents;
  
  public SSortPackagesByName() {
    indents = false;
  }
  
  
  @Override
  public void executeOn(SProject project) {
    project.accept(this);
  }
  
  @Override
  public void visitPackage(SPackage pkg) {    
    pkg.background();
  }
  
  @Override
  public void visitClass(SClass clss) {
    clss.setIndents(indents);
  }

  @Override
  public void visitProject(SProject project) {
    Collections.sort(project.packages());
    
  }

}