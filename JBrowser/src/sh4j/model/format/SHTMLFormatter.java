package sh4j.model.format;

import sh4j.model.highlight.SDummy;
import sh4j.model.highlight.SHighlighter;
import sh4j.model.style.SStyle;
import sh4j.parser.model.SBlock;
import sh4j.parser.model.SText;
/**
 * Give format for HTML, implements the interface SFormatters.
 */
public class SHTMLFormatter implements SFormatter {
  private final StringBuffer buffer;
  private final SStyle style;
  private final boolean numbering;
  private final SHighlighter[] highlighters;
  private int lines;
  private int level;
  /**
   * Use this to instantiate objects of the class SHTMLFormatter.
   * @param style Style to format with.
   * @param hilighters Highlighters to use.
   */
  public SHTMLFormatter(SStyle style, boolean numbering, SHighlighter... hilighters) {
    this.style = style;
    this.numbering = numbering;
    highlighters = hilighters;
    buffer = new StringBuffer();
    lines = 1;
  }

  private SHighlighter lookup(String text) {
    for (SHighlighter h : highlighters) {
      if (h.needsHighLight(text)) {
        return h;
      }
    }
    return new SDummy();
  }

  @Override
  public void styledWord(String word) {
    buffer.append(lookup(word).highlight(word, style));
  }

  @Override
  public void styledChar(char character) {
    buffer.append(lookup(character + "").highlight(character + "", style));
  }

  @Override
  public void styledSpace() {
    buffer.append(' ');
  }

  @Override
  public void styledCR() {
    buffer.append("\n");
    if (numbering) {
      String number = "<span style='background:#f1f0f0;'>  " + lines + " </span>";
      buffer.append(number);
    }
    lines++;
    indent();
  }

  @Override
  public void styledBlock(SBlock blockToStyle) {
    level++;
    for (SText text : blockToStyle.texts()) {
      text.export(this);
    }
    level--;
  }
  /**
   * Indent text.
   */
  public void indent() {
    for (int i = 0; i < level; i++) {
      buffer.append("  ");
    }
  }

  @Override
  public String formattedText() {
    return style.formatBody(buffer.toString());
  }
  
  public int getLines() {
    return lines;
  }
  
  public static String tag(String name, String content, String style) {
    return "<" + name + " style='" + style + "'>" + content + "</" + name + ">";
  }
}
