package sh4j.model.format;

import sh4j.parser.model.SBlock;
/**
 * Extend interface to create formatters.
 */
public interface SFormatter {
  public void styledWord(String word);

  public void styledChar(char character);

  public void styledSpace();

  public void styledCR();

  public void styledBlock(SBlock blockToStyle);

  public String formattedText();
}
