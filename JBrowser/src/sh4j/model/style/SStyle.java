package sh4j.model.style;
/**
 * Use this interface to create styles.
 */
public interface SStyle {

  public String toString();

  public String formatClassName(String text);

  public String formatCurlyBracket(String text);

  public String formatKeyWord(String text);

  public String formatPseudoVariable(String text);

  public String formatSemiColon(String text);

  public String formatString(String text);

  public String formatMainClass(String text);

  public String formatModifier(String text);

  public String formatBody(String text);


}
