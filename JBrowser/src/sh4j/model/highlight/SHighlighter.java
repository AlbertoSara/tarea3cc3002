package sh4j.model.highlight;

import sh4j.model.style.SStyle;
/**
 * Extend this interface to create more highlight.
 */
public interface SHighlighter {
  public boolean needsHighLight(String text);

  public String highlight(String text, SStyle style);
}
